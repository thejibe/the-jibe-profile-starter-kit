# The Jibe Profile Starter Kit

Kick off your next Drupal 8 project will all of your favourite configuration already in place. Use this repo as a starting point and then add in the customizations for your team.

### D8 Install Profile
See `d8-profile` branch

1. In the root directory of your Drupal site, enter the `profiles` folder.
1. Create a new folder called `thejibeprofilestarterkit` in this folder.
1. Download the contents of the `d8-profile` branch to this folder. This includes:
    * config
    * thejibeprofilestarterkit.install
    * thejibeprofilestarterkit.profile
    * thejibeprofilestarterkit.info.yml
    * thejibeprofilestarterkit.links.menu.yml
1. Optional: Rename all instances of `thejibeprofilestarterkit` to `yourprofile`.
1. Install Drupal and select **The Jibe Profile Starter Kit** install profile.

### D8 Make File
See `d8-make` branch

You can also use Druah make to pull in your install profile along with your
required contrib dependacies.

1. Clone `thejibeprofilestarterkit` repo to your webserver directory. `$ git clone git@bitbucket.org:thejibe/the-jibe-profile-starter-kit.git yoursitename`
1. Checkout the make file branch. `$ git checkout d8-make`
1. Run the Drush make file. `$ drush make thejibeprofilestarterkit.build`
1. Optional: Rename all instances of `thejibeprofilestarterkit` to `yourprofile`.
1. Proceed to install Drupal select **The Jibe Profile Starter Kit** install profile.
